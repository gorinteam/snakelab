﻿using System;
using System.Windows.Forms;

namespace snakeForLab
{
    public partial class Game : Form
    {
        private Point food = new Point();

        public Game()
        {
            InitializeComponent();

            // установим дефолтные настройки
            new Settings();

            // установим скорость и таймер
            gametimer.Interval = 1000 / Settings.Speed;
            gametimer.Tick += UpdateScreen;
            gametimer.Start();

            //Старит игры
            Start();

        }

        private void Start()
        {
            lblGameOver.Visible = false;
            
            //дефолтные настройки
            new Settings();

            Snake.New();

            lblScore.Text = Settings.Score.ToString();

            //Рандомно размещаем еду
            food.Generate(pbCanvas.Size.Width, pbCanvas.Size.Height);
        }

        private void UpdateScreen(object sender, EventArgs e)
        {
            //Проверим не проиграл ли игрок
            if (Settings.GameOver)
            {
                //Проверим не нажат ли Enter
                if (Input.KeyPressed(Keys.Enter))
                {
                    Start();
                }
            }
            else
            {
                if (Input.KeyPressed(Keys.Right) && Settings.Direction != Direction.Left)
                    Settings.Direction = Direction.Right;
                else if (Input.KeyPressed(Keys.Left) && Settings.Direction != Direction.Right)
                    Settings.Direction = Direction.Left;
                else if (Input.KeyPressed(Keys.Up) && Settings.Direction != Direction.Down)
                    Settings.Direction = Direction.Up;
                else if (Input.KeyPressed(Keys.Down) && Settings.Direction != Direction.Up)
                    Settings.Direction = Direction.Down;

                MovePlayer();

            }

            pbCanvas.Invalidate();
                
        }

        private void MovePlayer()
        {
            for (int i = Snake.Snk.Count - 1; i>=0;i--)
            {
                //движение головы змеи
                if (i==0)
                {
                    switch (Settings.Direction)
                    {
                        case Direction.Right:
                            Snake.Snk[i].X++;
                            break;
                        case Direction.Left:
                            Snake.Snk[i].X--;
                            break;
                        case Direction.Down:
                            Snake.Snk[i].Y++;
                            break;
                        case Direction.Up:
                            Snake.Snk[i].Y--;
                            break;
                    }

                    //максимальные значение по x и y
                    int maxX = pbCanvas.Size.Width / Point.Width;
                    int maxY = pbCanvas.Size.Height / Point.Height;

                    //столкновения cо стенами
                    if (Snake.Snk[i].X < 0 || Snake.Snk[i].Y < 0 || Snake.Snk[i].X>=maxX || Snake.Snk[i].Y>=maxY)
                    {
                        Snake.Die();
                    }

                    //столкновение с телом
                    for (int j = 1; j < Snake.Snk.Count; j++)
                    {
                        if (Snake.Snk[i].X == Snake.Snk[j].X && Snake.Snk[i].Y == Snake.Snk[j].Y)
                        {
                            Snake.Die();
                        }
                    }

                    //столкновение с едой
                    if (Snake.Snk[0].X == food.X && Snake.Snk[0].Y == food.Y)
                    {
                        //Змейка растет
                        Snake.GrowUp();

                        //Увеличить счетчик
                        Settings.Score += Settings.Points;
                        lblScore.Text = Settings.Score.ToString();

                        food.Generate(pbCanvas.Size.Width, pbCanvas.Size.Height);
                    }
                }
                else
                {
                    Snake.Snk[i].X = Snake.Snk[i - 1].X;
                    Snake.Snk[i].Y = Snake.Snk[i - 1].Y;
                }
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Input.ChangeState(e.KeyCode, true);
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            Input.ChangeState(e.KeyCode, false);
            exitBtn.Enabled = false;

        }

        private void PbCanvas_Paint(object sender, PaintEventArgs e)
        {
            if (!Settings.GameOver)
            {
                //нарисовать змейку
                Snake.DrawSnk(e);
                //рисем еду
                food.Draw(e);
            }
            else
            {
                string gameOver = "Игра окончена \nВы набрали: " + Settings.Score + "\nНажмите Enter\nчтобы начать сначала";
                lblGameOver.Text = gameOver;
                lblGameOver.Visible = true;
                exitBtn.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
