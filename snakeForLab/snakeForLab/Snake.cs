﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace snakeForLab
{
    class Snake : Point
    {
        public static List<Point> Snk = new List<Point>();

        public static void Die()
        {
            Settings.GameOver = true;
        }

        public static void New()
        {
            Point head = new Point
            {
                X = 10,
                Y = 5
            };
            Snk.Clear();
            Snk.Add(head);
        }

        public static void GrowUp()
        {
            Point snake = new Point
            {
                X = Snk[Snk.Count - 1].X,
                Y = Snk[Snk.Count - 1].Y
            };
            Snk.Add(snake);
        }

        public static void DrawSnk(PaintEventArgs e)
        {
            for (int i = 0; i < Snk.Count; i++)
            {
                //цвет змейки
                Brush snakeColour;
                if (i == 0) snakeColour = Brushes.Black; //голова
                else snakeColour = Brushes.Green;        //тело

                Snk[i].Draw(snakeColour, e);
            }
        }
    }


}
