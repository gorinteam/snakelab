﻿
namespace snakeForLab
{

    public enum Direction { Up, Down, Left, Right };

    class Settings
    {
        public static int Speed { get; set; }
        public static int Score { get; set; }
        public static int Points { get; set; }
        public static bool GameOver { get; set; }
        public static Direction Direction { get; set; }

        public Settings()
        {
            Speed = 10;
            Score = 0;
            Points = 100;
            GameOver = false;
            Direction = Direction.Down;
        }
    }
}
