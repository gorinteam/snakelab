﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace snakeForLab
{
    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        public static int Width { get; set; }
        public static int Height { get; set; }

        private Brush color;

        public Point()
        {
            X = 0;
            Y = 0;
            color = Brushes.Red;
            Width = 15;
            Height = 15;
        }

        public void Draw(Brush color,PaintEventArgs e)
        {
            Graphics paint = e.Graphics;
            paint.FillEllipse(color, new Rectangle(X * Width, Y * Height, Width, Height));

        }

        public void Draw(PaintEventArgs e)
        {
            Graphics paint = e.Graphics;
            paint.FillEllipse(color, new Rectangle(X * Width, Y * Height, Width, Height));

        }
        
        public void Generate(int pbWidth, int pbHeight)
        {
            int maxX = pbWidth / Width;
            int maxY = pbHeight / Height;
            Random random = new Random();
            X = random.Next(0, maxX);
            Y = random.Next(0, maxY);
        }
        
    }
}
